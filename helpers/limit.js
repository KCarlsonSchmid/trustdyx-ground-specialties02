// You can extend the core handlebars functionality by adding helpers.
// Any files in this directory will be picked up automatically.
module.exports = {
    limit: function (arr, limit) {
        return arr.slice(0, limit);
    }
};